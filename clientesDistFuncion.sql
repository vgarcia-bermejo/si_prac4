﻿
	drop function if exists clientesdistintosfunct(years int, months int, umbr numeric);


	create function clientesdistintosfunct(years int, months int, umbr numeric) returns int as $$
		Declare
		i integer;
		Begin
		i = (Select count (distinct customerid) from orders where
				extract(year from orderdate):: integer = years and
				extract(month from orderdate)::integer < months and totalamount > umbr) as integer;
		return i;
		end;
	$$ Language plpgsql;

select clientesdistintosfunct(2014, 4, 100)

