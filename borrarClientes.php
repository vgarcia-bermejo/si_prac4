<?php
define("PGUSER", "alumnodb");
define("PGPASSWORD", "alumnodb");
define("DSN","pgsql:host=localhost;dbname=si1;options='--client_encoding=UTF8'");
?>

<html>
  <head>
    <title>Lista clientes por mes</title>
      <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <style type="text/css">
        table {
                border-style: none;
                border-collapse: collapse;
        }
        table th {
                border-width: 1px;
                padding: 1px;
                border-style: solid;
                border-color: gray;
                background-color: rgb(230, 230, 220);
        }
        table td {
                border-width: 1px;
                padding: 1px;
                border-style: solid;
                border-color: gray;
                background-color: rgb(255, 255, 240);
        }
      </style>
  </head>
  <body>
    <h2>Lista de clientes por mes</h2>

    <?php

      $db = new PDO(DSN,PGUSER,PGPASSWORD);
      if (!isset($_REQUEST['id'])) {
    ?>
    <form action="">

      <h4>Parámetros del listado:</h4>
      <table>
        <tr><td>Introduce Id del customer:</td><td><input type="text" name="id" value="Id"></td></tr>
      </table>
      <input type="submit" name="fecha" value="Enviar">
    </form>

    <?php
      } else {
        try {

          $db->beginTransaction();

          $consulta = "delete from orderdetail where orderid in (select orderid from orders where customerid =".$_REQUEST['id'].")";
          $stmt = $db->exec($consulta);
          if($stmt === FALSE){
              $db->rollBack();
              die();
          }
          $consulta2 = "delete from orders where customerid = ".$_REQUEST['id'];
          $stmt = $db->exec($consulta2);
          if($stmt === FALSE){
              $db->rollBack();
              die();
          }

          $consulta3 = "delete from customers where customerid = ".$_REQUEST['id'];
          sleep(100);
          $stmt = $db->exec($consulta3);
          if($stmt === FALSE){
              $db->rollBack();
              die();
          }
          $db->commit();

          echo "<h3>El usuario".$_REQUEST['id']." se ha borrado correctamente</h3>";

          // Impresion de resultados en HTML
        } catch (PDOException $e) {
          $db->rollback();
          echo "<h3>NO SE HA PODIDO BORRAR CORRECTAMENTE ESE USUARIO</h3>";
        }

        $db = null;
      }
    ?>
  </body>
</html>
