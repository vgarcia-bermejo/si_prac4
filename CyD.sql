﻿create index guillenoob  on orders(totalamount);


explain select customerid from customers where customerid not in (select customerid from orders where status='Paid');

explain select customerid from (select customerid from 
	customers union all select customerid from orders where status='Paid')
	as A group by customerid having count(*) =1;


explain select customerid from customers except select customerid from orders where status='Paid';

create index ind  on orders(status);

drop index ind;

analyze orders;

explain select count(*) from orders where status is null;


explain select count(*) from orders where status ='Shipped';

explain select count(*) from orders where status ='Paid';

explain select count(*) from orders where status ='Processed';
