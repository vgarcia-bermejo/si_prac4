

Alter table customers add column promo numeric;

update customers set promo = 0;

--alter table customers drop column promo;


create or replace function updPromof() returns trigger as $$
	Begin
		Update orderdetail set price = products.price - products.price * ( customers.promo/100)
		from customers, products, orders where  orders.status = null and orders.customerid = customers.customerid and
		orders.orderid = orderdetail.orderid and orderdetail.prod_id = products.prod_id;

		perform pg_sleep(15);

		return new;
	End;
$$
Language plpgsql;


CREATE TRIGGER updPrice after update on customers
FOR EACH ROW EXECUTE PROCEDURE updPromof();



update orders set status = null where customerid = 73;
update customers set promo = 50 where customerid = 73;

