-- Victor Garcia-bermejo y Javier Encinas


--Creamos el indice
Create Index idx_totam on orders(totalamount);


--Consulta simple con el explain
﻿explain Select count (distinct customerid) from orders where extract(year from orderdate):: integer = 2014 and extract(month from orderdate)::integer < 4 and totalamount > 100;

